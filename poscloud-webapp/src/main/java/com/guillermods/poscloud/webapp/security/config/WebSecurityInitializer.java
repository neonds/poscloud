/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.poscloud.webapp.security.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * @author Guillermo B Díaz Solís
 * @date 25 de dic. de 2015
 */
public class WebSecurityInitializer extends AbstractSecurityWebApplicationInitializer {

}
