/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.poscloud.service.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.guillermods.poscloud.repository.config.DatabaseConfig;

/**
 * @author Guillermo B Díaz Solís
 * @date 25 de dic. de 2015
 */
@Configuration
@ComponentScan(basePackages = {"com.guillermods.poscloud.service"})
@Import(value = {DatabaseConfig.class})
public class ApplicationConfig {

}
