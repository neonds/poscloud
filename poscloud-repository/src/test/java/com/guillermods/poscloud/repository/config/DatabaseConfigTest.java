/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.poscloud.repository.config;

import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Guillermo B Díaz Solís
 * @date 25 de dic. de 2015
 * 
 * Application Config for H2 database
 * 
 */
@Configuration
@ComponentScan("com.guillermods.poscloud.repository.dao")
@EnableTransactionManagement
public class DatabaseConfigTest {

	/**
	 * 
	 */
	private static final String JDBC_H2 = "jdbc:h2:mem:migrationtestdb;DB_CLOSE_DELAY=5;TRACE_LEVEL_SYSTEM_OUT=1";

	/**
	 * Creation of H2 db
	 * 
	 * @return A new Instance of DataSource
	 */
	@Bean
	public DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName("org.h2.Driver");
		dataSource.setUrl(JDBC_H2);
		dataSource.setUsername("sa");
		dataSource.setPassword("");
		return dataSource;
	}

	/**
	 * 
	 * @return Flyway
	 */
	@Bean
	public Flyway flyway() {
		Flyway flyway = new Flyway();
		flyway.setLocations("classpath:db/migration");
		flyway.setDataSource(this.dataSource());
		flyway.migrate();
		return flyway;
	}
}
