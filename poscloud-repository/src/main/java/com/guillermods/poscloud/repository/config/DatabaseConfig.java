/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */

package com.guillermods.poscloud.repository.config;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.lookup.JndiDataSourceLookup;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Guillermo B Díaz Solís
 * @date 25 de dic. de 2015
 */
@Configuration
@ComponentScan("com.guillermods.poscloud.repository.dao")
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
public class DatabaseConfig {

	@Autowired
	private Environment env;

	/**
	 * @return DataSource
	 * @throws SQLException
	 */
	@Bean
	public DataSource dataSource() throws SQLException {
		final JndiDataSourceLookup dsLookup = new JndiDataSourceLookup();
		dsLookup.setResourceRef(true);
		DataSource dataSource = dsLookup.getDataSource(env.getProperty("app.database.jndi"));
		return dataSource;
	}

	/**
	 * @param dataSource
	 * @return DataSourceTransactionManager
	 */
	@Bean
	public DataSourceTransactionManager getDataSourceTransactionManager(final DataSource dataSource) {
		DataSourceTransactionManager transManager = new DataSourceTransactionManager();
		transManager.setDataSource(dataSource);
		return transManager;

	}

}
