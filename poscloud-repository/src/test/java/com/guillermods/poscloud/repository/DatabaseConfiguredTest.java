/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.poscloud.repository;

import static org.junit.Assert.*;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.guillermods.poscloud.repository.config.DatabaseConfigTest;

/**
 * @author Guillermo B Díaz Solís
 * @date 25 de dic. de 2015
 * 
 * Case for test the application config test configuration
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { DatabaseConfigTest.class }, loader = AnnotationConfigContextLoader.class)
public class DatabaseConfiguredTest {
	
	@Autowired
	DataSource datasource;
	
	@Test
	public void testDatasource(){
		assertNotNull(datasource);
		
	} 
}
