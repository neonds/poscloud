/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.poscloud.webapp.controllers;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Guillermo B Díaz Solís
 * @date 25 de dic. de 2015
 */
@Controller
@RequestMapping("/welcome")
public class WelcomeController {

	class Test{
		private String id;
		
		public void setId(String id){
			this.id = id;
		}
		
		public String getId(){
			return this.id;
		}
	}
	
	@RequestMapping("/")
	public String index(@RequestParam Map<String, String> params, Model model){
		
		Test t1 = new Test();
		t1.setId("A");
		
		Test t2= new Test();
		t2.setId("B");
		
		Test t3 = new Test();
		t3.setId("C");
		
		Test t4 = new Test();
		t4.setId("D");
		
		Test [] listTest = new Test[4];
		listTest[0] = t1;
		listTest[1] = t2;
		listTest[2] = t3;
		listTest[3] = t4;
		
		model.addAttribute("listTest", listTest);
		return "welcome";
	}
}
