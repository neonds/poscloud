/**
 * Copyright (C) 2015 Guillermo Díaz Solís.
 * Todos los derechos reservados.
 */
package com.guillermods.poscloud.webapp.controllers;

import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Guillermo B Díaz Solís
 * @date 26 de dic. de 2015
 */

@Controller
public class HomeController {
	
	@RequestMapping("")
	public String index(@RequestParam Map<String, String> param, Model model){
		
		if (!param.isEmpty()){
			System.out.println(param.get("val"));
		}
		
		return "welcome";
	}
}
